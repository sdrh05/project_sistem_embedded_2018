int redPin = 2; 
int yellowPin = 3; 
int greenPin = 4; 
int lama_merah = 10000, jeda_merah_hijau = 1000,
    lama_hijau = 10000, jeda_hijau_merah = 1000;
void setup() 
{ pinMode(redPin, OUTPUT); 
  pinMode(yellowPin, OUTPUT); 
  pinMode(greenPin, OUTPUT); 
   
}

void loop() 
{ //nyala lampu merah 
  //state = 0 
  setLights(HIGH, LOW, LOW);
  delay(lama_merah);
  //state = 1 
  setLights(HIGH, HIGH, LOW);
  delay(jeda_merah_hijau);  
  //state = 2 
  setLights(LOW, LOW, HIGH);
  delay(lama_hijau); 
  //state = 3 
  setLights(LOW, HIGH, LOW);
  delay(jeda_hijau_merah); 
  //========================= 
  delay(1000); 
  }
  
void setLights(int red, int yellow, int green) 
{ digitalWrite(redPin, red); 
  digitalWrite(yellowPin, yellow); 
  digitalWrite(greenPin, green); 
 }
