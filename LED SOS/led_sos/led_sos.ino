int ledPin =12;
void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, HIGH);   
  // S (...) first dot 
  delay(200); digitalWrite(ledPin, LOW); delay(200); 
  digitalWrite(ledPin, HIGH);  
  // second dot 
  delay(200); 
  digitalWrite(ledPin, LOW); delay(200); 
  digitalWrite(ledPin, HIGH);  
  // third dot 
  delay(200); 
  digitalWrite(ledPin, LOW); delay(500); 
  digitalWrite(ledPin, HIGH);   
  // O (—-) first dash 
  delay(500); digitalWrite(ledPin, LOW); delay(500); digitalWrite(ledPin, HIGH);   
  // second dash 
  delay(500); digitalWrite(ledPin, LOW); delay(500); digitalWrite(ledPin, HIGH);   
  // third dash 
  delay(500); digitalWrite(ledPin, LOW); delay(500); digitalWrite(ledPin, HIGH);   
  // S (...) first dot 
  delay(200); digitalWrite(ledPin, LOW); delay(200); digitalWrite(ledPin, HIGH);   
  // second dot 
  delay(200); digitalWrite(ledPin, LOW); delay(200); digitalWrite(ledPin, HIGH);   
  // third dot 
  delay(200); digitalWrite(ledPin, LOW); 
  delay(1000);                  
  // wait 1 second before we start again
}
