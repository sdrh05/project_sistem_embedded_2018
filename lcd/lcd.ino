#include <LiquidCrystal.h>
 // LiquidCrystal display with: // rs on pin 12 // rw on pin 11 // enable on pin 10 // d4-7 on pins 5-2 
LiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2);
void setup() { 
  Serial.begin(9600); 
  lcd.begin(2, 20); 
  lcd.clear(); 
  lcd.setCursor(0,0); 
  lcd.print("LCD project \x7e\x7e\x7e"); 
  lcd.setCursor(0,1); 
  lcd.print("Rules:# clr / ln"); }
  
void loop() { 
  if (Serial.available()) { 
    char ch = Serial.read(); 
    if (ch == '#') { lcd.clear(); }
    else if (ch == '/') { 
      lcd.setCursor(0,1); } 
    else { lcd.write(ch); } 
    } 
    }
